#include <iostream>
#include <string>
#include <time.h>
#include "Node.h"

using namespace std;

Node* randomStartPoint(Node* watchers, int& size);
Node* passCloak(Node* watchers, int size);
Node* createSightseer(int size);
Node* deleteSightseer(Node* watchers, int& size);
void printSightseer(Node* watchers, int size);

int main()
{

	srand(time(NULL));
	int size = 0;
	int round = 1;
	Node* head = new Node;

	//Get the size of the list
	cout << "How many watchers are there? ";
	cin >> size;

	//create a circular linked list
	head = createSightseer(size);

	//randomize a starting point
	head = randomStartPoint(head, size);
	system("cls");

	while (size != 1)
	{

		cout << "==========" << endl << "Round: " << round << endl << "==========" << endl;

		//display the list of Sightseers
		printSightseer(head, size);

		head = passCloak(head, size);
		head = deleteSightseer(head, size);

		round++;
		system("pause");
		system("cls");

	}

	// Remaining Sightseer for reinforcements
	cout << "==========" << endl << "Final Result" << endl << "==========" << endl;
	cout << head -> name << " will go to seek for reinforcements" << endl;

	delete head;
	head = nullptr;


	system("pause");
	return 0;

}

Node* createSightseer(int size)
{

	// Declare variables for a linked list
	Node* head = nullptr;
	Node* current = nullptr;
	Node* previous = nullptr;
	string name;

	// Loop circular linked list (any size)
	for (int i = 0; i < size; i++)
	{

		// Asking for the soldiers' names
		cout << "Input name for your soldier: ";
		cin >> name;

		// Node creation
		current = new Node;
		current -> name = name;

		// Set node as the head if it is the first node
		if (i == 0)
		{

			head = current;

		}

		// Set previous node's next node to the current node if applicable
		if (i > 0)
		{

			current -> previous = previous;
			previous -> next = current;

		}

		// Current as previous node for next node
		previous = current;

		// Set next node to the head node if current node is the last node
		if (i == size - 1)

		{

			current->next = head;

		}

	}

	// Set the head's previous node to the last node
	head -> previous = previous;

	return head;

}

// Remove the Sightseer rolled from the list
Node* deleteSightseer(Node* watchers, int& size)
{

	Node* toDelete = watchers;
	Node* previous = nullptr;

	for (int i = 0; i < size; i++)
	{

		previous = toDelete;
		toDelete = toDelete->next;

	}

	previous -> next = toDelete->next;
	previous = previous->next;

	// Display before removing the eliminated Sightseer 
	cout << toDelete -> name << " was eliminated" << endl << endl;

	size -= 1;

	delete toDelete;
	return previous;

}

// Pass the cloak randomly (list size based)
Node* passCloak(Node* watchers, int size)
{
	int randomSightseer = rand() % size + 1;
	int count = 0;

	cout << endl << watchers -> name << " has the cloak and has drawn " << randomSightseer << endl;

	//Pass the cloak until the count matches the random number 
	while (randomSightseer != count)
	{

		watchers = watchers->next;
		count++;

	}

	return watchers;

}

// Pick a random Sightseer to be the starting point
Node* randomStartPoint(Node* watchers, int& size)
{

	int randomNumber = rand() % size;

	for (int i = 0; i < randomNumber; i++)
	{
		watchers = watchers->next;
	}

	return watchers;

}

// Display the remaining watchers
void printSightseer(Node* watchers, int size)
{

	Node* printWatchers = watchers;

	for (int i = 0; i < size; i++)

	{

		cout << printWatchers -> name << endl;
		printWatchers = printWatchers -> next;

	}
}

