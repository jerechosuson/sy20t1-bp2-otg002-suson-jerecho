#pragma once
#include <string>
#include <iostream>

using namespace std;

class Ai;

class Pl

{

public:

	int getHp(); // health
	int maxHp();
	int setHp(float damage);
	int getAgi(); // ability to move quickly and easily.
	int getDex(); // contributes to physical skills
	int getPwr(); // raw damage
	int getVit();  // contributes to life/energy
	string getRole();
	string getName();
	void printStats();
	void createPlayer(string name, int roleNumber);
	void gainStats(Ai* target); // bonus evaluation after the round
	void calHr(Ai* target); // hit rate calculation
	void calDmg(Ai* target); // attack damage calculation

private:

	string mName;
	string mRole;
	int mHp;
	int mMaxHp;
	int mAgi;
	int mDex;
	int mPwr;
	int mVit;

};
