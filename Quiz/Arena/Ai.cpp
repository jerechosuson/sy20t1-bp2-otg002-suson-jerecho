#include "Ai.h"
#include "Pl.h"

void Ai::createAi(int roleNumber)
{
	if (roleNumber == 1) // Typical warrior stats, nullify enough damage but having low damage
	{
		// Warrior is hp dependent
		mRole = "Warrior";
		mHp = 18;
		mAgi = 3;
		mDex = 4;
		mPwr = 8;
		mVit = 4;
		
	}

	else if (roleNumber == 2)
	{

		// Mage is versatile
		mRole = "Mage";
		mHp = 15;
		mAgi = 6;
		mDex = 7;
		mPwr = 6;
		mVit = 5;
	
	}

	else if (roleNumber == 3)
	{

		// Asassin is Fast but squishy in replacement of damage
		mRole = "Assassin";
		mHp = 12;
		mAgi = 9;
		mDex = 6;
		mPwr = 10;
		mVit = 4;
	
	}
}

// this will compute hit rate
void Ai::calHr(Pl* target)
{
	int hitRate = ((float)mDex / (float)target->getAgi()) * 100;
	int probability = rand() % 100 + 1;
	if (hitRate < 20) hitRate = 20;
	else if (hitRate > 80) hitRate = 80;

	if (probability >= 1 && probability <= hitRate) // call damage function if hit successfully
	{
		calDmg(target);
	}

	else if (probability > hitRate) // display missed!
	{
		cout << "Enemy missed!" << endl;
	}
}

// computes attack damage
void Ai::calDmg(Pl* target)
{
	if (mRole == "Warrior" && target->getRole() == "Assassin") // extra damage for warrior vs assassin
	{

		float damage = (mPwr - target->getVit()) * 1.5;
		if (damage <= 0) damage = 1; // minimum dmg is 1

		target->setHp(damage);
		cout << "The enemy attacked " << target->getName() << " for " << damage << "!" << endl;

	}

	else if (mRole == "Mage" && target->getRole() == "Warrior") // extra damage for mage vs warrior
	{

		float damage = (mPwr - target->getVit()) * 1.5;
		if (damage <= 0) damage = 1;

		target->setHp(damage);
		cout << "The enemy attacked " << target->getName() << " for " << damage << "!" << endl;

	}

	else if (mRole == "Assassin" && target->getRole() == "Mage") // extra damage for assassin vs mage
	{

		float damage = (mPwr - target->getVit()) * 1.5;
		if (damage <= 0) damage = 1;

		target->setHp(damage);
		cout << "The enemy attacked " << target->getName() << " for " << damage << "!" << endl;

	}


	else // damage calculation if no incentive apply
	{

		float damage = (mPwr - target->getVit());
		if (damage <= 0) damage = 1;

		target->setHp(damage);
		cout << "The enemy attacked " << target->getName() << " for " << damage << "!" << endl;

	}
}

// Opponent difficulty
void Ai::enhanceOpponent(int diff)
{

	if (diff == 2)
	{

		mHp += ((float)mHp * 0.2);
		mAgi += ((float)mAgi * 0.2);
		mDex += ((float)mDex * 0.2);
		mPwr += ((float)mPwr * 0.2);
		mVit += ((float)mVit * 0.2);
		
	}


	else if (diff == 3)
	{

		mHp += ((float)mHp * 0.2) * 2;
		mAgi += ((float)mAgi * 0.2) * 2;
		mDex += ((float)mDex * 0.2) * 2;
		mPwr += ((float)mPwr * 0.2) * 2;
		mVit += ((float)mVit * 0.2) * 2;

	}

	else if (diff == 4)
	{

		mHp += ((float)mHp * 0.2) * 3;
		mAgi += ((float)mAgi * 0.2) * 3;
		mDex += ((float)mDex * 0.2) * 3;
		mPwr += ((float)mPwr * 0.2) * 3;
		mVit += ((float)mVit * 0.2) * 3;
		
	}


	else if (diff == 5)
	{

		mHp += ((float)mHp * 0.2) * 4;
		mAgi += ((float)mAgi * 0.2) * 4;
		mDex += ((float)mDex * 0.2) * 4;
		mPwr += ((float)mPwr * 0.2) * 4;
		mVit += ((float)mVit * 0.2) * 4;
		
	}


	else if (diff == 6)
	{

		mHp += ((float)mHp * 0.2) * 5;
		mAgi += ((float)mAgi * 0.2) * 5;
		mDex += ((float)mDex * 0.2) * 5;
		mPwr += ((float)mPwr * 0.2) * 5;
		mVit += ((float)mVit * 0.2) * 5;

	}

	else if (diff == 7)
	{

		mHp += ((float)mHp * 0.2) * 6;
		mAgi += ((float)mAgi * 0.2) * 6;
		mDex += ((float)mDex * 0.2) * 6;
		mPwr += ((float)mPwr * 0.2) * 6;
		mVit += ((float)mVit * 0.2) * 6;

	}

	else if (diff == 8)
	{

		mHp += ((float)mHp * 0.2) * 7;
		mAgi += ((float)mAgi * 0.2) * 7;
		mDex += ((float)mDex * 0.2) * 7;
		mPwr += ((float)mPwr * 0.2) * 7;
		mVit += ((float)mVit * 0.2) * 7;

	}

	else if (diff == 9)
	{

		mHp += ((float)mHp * 0.2) * 8;
		mAgi += ((float)mAgi * 0.2) * 8;
		mDex += ((float)mDex * 0.2) * 8;
		mPwr += ((float)mPwr * 0.2) * 8;
		mVit += ((float)mVit * 0.2) * 8;
	
	}

	else if (diff >= 10)
	{

		mHp += ((float)mHp * 0.2) * 9;
		mAgi += ((float)mAgi * 0.2) * 9;
		mDex += ((float)mDex * 0.2) * 9;
		mPwr += ((float)mPwr * 0.2) * 9;
		mVit += ((float)mVit * 0.2) * 9;

	}
}

void Ai::printStats()
{

	cout << "Class: " << mRole << endl;
	cout << "STATS: " << endl;
	cout << "HP: " << mHp << endl;
	cout << "Agility: " << mAgi << endl;
	cout << "Dexterity: " << mDex << endl;
	cout << "Power: " << mPwr << endl;
	cout << "Vitality: " << mVit << endl;

}

int Ai::getHp()
{
	return mHp;
}

int Ai::setHp(float damage)
{
	mHp -= damage;
	if (mHp < 0) mHp = 0;
	return mHp;
}

int Ai::getAgi()
{
	return mAgi;
}

int Ai::getDex()
{
	return mDex;
}

int Ai::getPwr()
{
	return mPwr;
}

int Ai::getVit()
{
	return mVit;
}

string Ai::getRole()
{
	return mRole;
}
