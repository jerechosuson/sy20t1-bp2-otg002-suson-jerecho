#include "Pl.h"
#include "Ai.h"

string Pl::getName()
{
	return mName;
}

void Pl::printStats()
{

	cout << "STATS: " << endl;
	cout << "Name: " << mName << endl;
	cout << "HP: " << mHp << endl;
	cout << "Class: " << mRole << endl;
	cout << "Agility: " << mAgi << endl;
	cout << "Dexterity: " << mDex << endl;
	cout << "Power: " << mPwr << endl;
	cout << "Vitality: " << mVit << endl;

}

int Pl::getHp()
{
	return mHp;
}

int Pl::maxHp()
{
	return mMaxHp;
}

int Pl::setHp(float damage)
{
	mHp -= damage;
	if (mHp < 0) mHp = 0;
	return mHp;
}

int Pl::getAgi()
{
	return mAgi;
}

int Pl::getDex()
{
	return mDex;
}

int Pl::getPwr()
{
	return mPwr;
}

int Pl::getVit()
{
	return mVit;
}

string Pl::getRole()
{
	return mRole;
}

void Pl::createPlayer(string name, int roleNumber)
{
	mName = name;

	// Warrior is hp dependent
	if (roleNumber == 1)
	{

		// Warrior is hp dependent
		mRole = "Warrior";
		mHp = 18;
		mMaxHp = 18;
		mAgi = 3;
		mDex = 4;
		mPwr = 8;
		mVit = 4;

	}

	else if (roleNumber == 2)
	{

		// Mage is versatile
		mRole = "Mage";
		mHp = 15;
		mMaxHp = 15;
		mAgi = 6;
		mDex = 7;
		mPwr = 6;
		mVit = 5;
	
	}

	else if (roleNumber == 3)
	{

		// Asassin is Fast but squishy in replacement of damage
		mRole = "Assassin";
		mHp = 12;
		mMaxHp = 12;
		mAgi = 9;
		mDex = 6;
		mPwr = 10;
		mVit = 4;
	
	}
}

//this computes hit rate
void Pl::calHr(Ai* target)
{
	int hitRate = ((float)mDex / (float)target->getAgi()) * 100;
	int probability = rand() % 100 + 1;
	if (hitRate < 20) hitRate = 20;
	else if (hitRate > 80) hitRate = 80;

	if (probability >= 1 && probability <= hitRate)
	{
		calDmg(target);
	}

	else if (probability > hitRate)
	{
		cout << mName << " missed!" << endl;
	}

}

// attack multiplier computations
void Pl::calDmg(Ai* target)
{
		// attack multiplier for warrior vs assassin
	if (mRole == "Warrior" && target->getRole() == "Assassin")
	{

		float damage = (mPwr - target->getVit()) * 1.5;
		if (damage <= 0) damage = 1;

		target->setHp(damage);
		cout << mName << " has attacked the enemy " << target->getRole() << " for " << damage << "!" << endl;

	}

		// attack multiplier for assassin vs mage
	else if (mRole == "Assassin" && target->getRole() == "Mage")
	{

		float damage = (mPwr - target->getVit()) * 1.5;
		if (damage <= 0) damage = 1;

		target->setHp(damage);
		cout << mName << " has attacked the enemy " << target->getRole() << " for " << damage << "!" << endl;

	}
		// attack multiplier for mage vs warrior
	else if (mRole == "Mage" && target->getRole() == "Warrior")
	{

		float damage = (mPwr - target->getVit()) * 1.5;
		if (damage <= 0) damage = 1;

		target->setHp(damage);
		cout << mName << " has attacked the enemy " << target->getRole() << " for " << damage << "!" << endl;

	}

	else
	{

		float damage = (mPwr - target->getVit());
		if (damage <= 0) damage = 1;

		target->setHp(damage);
		cout << mName << " has attacked the enemy " << target->getRole() << " for " << damage << "!" << endl;

	}
}

void Pl::gainStats(Ai* target)
{
	//player health up
	mHp = mHp + ((float)mMaxHp * 0.3);

	if (target->getRole() == "Warrior") // benefit if you owned a warrior
	{

		cout << "Rewards" << endl << "Power +3" << endl << "Vitality +3" << endl;
		mPwr += 3;
		mVit += 3;

	}

	if (target->getRole() == "Mage") // benefit if you owned a mage
	{

		cout << "Reward" << endl << "Power +5" << endl;
		mPwr += 5;

	}

	else if (target->getRole() == "Assassin") // benefit if you owned an assassin
	{

		cout << "Rewards" << endl << "Agility +3" << endl << "Dexterity +3" << endl;
		mAgi += 3;
		mDex += 3;

	}

}
