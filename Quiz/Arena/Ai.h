#pragma once
#include <string>
#include <iostream>

using namespace std;

class Pl;

class Ai

{

public:

	int getHp(); // health
	int setHp(float damage);
	int getAgi(); // ability to move quickly and easily.
	int getDex(); // contributes to physical skills
	int getPwr(); // raw damage
	int getVit();  // contributes to life/energy
	string getRole();
	void printStats();
	void createAi(int roleNumber);
	void calHr(Pl* target);  // hit rate calculation
	void calDmg(Pl* target); // attack damage calculation
	void enhanceOpponent(int diff); // opponent difficulty

private:

	int mHp;
	int mAgi;
	int mDex;
	int mPwr;
	int mVit;
	string mRole;

};



