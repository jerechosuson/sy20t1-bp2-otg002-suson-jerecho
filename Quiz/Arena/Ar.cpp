#include "Pl.h"
#include "Ai.h"
#include <time.h>
#include <string>
#include <iostream>

using namespace std;

int main()
{
	srand(time(NULL));

	int diff = 1;	//Difficulty
	int roleNumber;
	string name;

		//Character preparation
	cout << "We now somehow have a worthy rival. What's your name homie?" << endl;
	cin >> name;
	system("cls");
	cout << " Choose your role: 1 for Warrior, 2 for Mage, 3 for Assassin" << endl << "[1] Warrior" << endl << "[2] Mage" << endl << "[3] Assassin" << endl;
	cin >> roleNumber;

		//Create player scenario
	Pl* player = new Pl();
	player->createPlayer(name, roleNumber);
	system("cls");

	while (player->getHp() > 0)
	{
		int enemyClassRandomizer = rand() % 3 + 1;
		//Opponent preparation
		Ai* opponent = new Ai();
		opponent->createAi(enemyClassRandomizer);

		if (diff > 1) opponent->enhanceOpponent(diff); // continue raising opponent's difficulty for succeeding rounds
		//Stats of opponent and player
		cout << "Difficulty: " << diff << endl;
		player->printStats();
		cout << endl << endl << "Versus" << endl << endl;
		opponent->printStats();
		cout << endl;

		system("pause");
		system("cls");

		while (opponent->getHp() > 0 && player->getHp() > 0)
		{

			//If Ai has higher AGI, Player attacks
			if (player->getAgi() > opponent->getAgi())
			{

				player->calHr(opponent);
				cout << endl;
				system("pause");
				cout << endl;

				if (opponent->getHp() > 0)
				{

					opponent->calHr(player);
					cout << endl;
					system("pause");

				}
			}

			//If higher AGI, Opponent atks
			else if (player->getAgi() < opponent->getAgi())
			{
				opponent->calHr(player);
				cout << endl;
				system("pause");
				cout << endl;

				if (player->getHp() > 0)
				{

					player->calHr(opponent);
					cout << endl;
					system("pause");

				}
			}

			//If AGI is equal, Player attacks 
			else if (player->getAgi() == opponent->getAgi())
			{
				player->calHr(opponent);
				cout << endl;
				system("pause");
				cout << endl;

				if (opponent->getHp() > 0)
				{

					opponent->calHr(player);
					cout << endl;
					system("pause");

				}
			}
		}
		system("cls");
		cout << "Player HP: " << player->getHp() << endl << "Enemy HP: " << opponent->getHp() << endl;
		cout << endl;

		if (player->getHp() > 0) // call victory evaluation if player is still alive
		{

			player->gainStats(opponent);
			cout << endl;
			cout << "End of round " << diff << endl;
			cout << endl;
			diff++;
			system("pause");
			system("cls");

		}
		//Delete opponent after the round
		delete opponent;
	}

	//If player loses
	if (player->getHp() <= 0)
	{

		cout << endl;
		cout << "==========" << "GAME OVER!" << endl;
		cout << player->getName() << " knock out after struggling " << diff << " round difficulty(ies) in the arena" << "==========" << endl;

	}

	delete player; // return player after the game
	system("pause");
	return 0;
}