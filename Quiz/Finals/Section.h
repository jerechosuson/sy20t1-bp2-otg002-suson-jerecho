#pragma once
#include <string>

using namespace std;

class Player;
class Opponent;

class Section
{
public:
	Section();

	// accessors
	int getAgi();
	int getDex();
	int getPower();
	int getHp();
	int setHp(int damage);
	
	string getClass();

	// behaviors
	virtual void attack(Section* objective);
	virtual void printStats();

protected:
	int mPow;
	int mVit;
	int mAgi;
	int mDex;
	int mHp;
	int mMaxHp;
	string mClass;
};

