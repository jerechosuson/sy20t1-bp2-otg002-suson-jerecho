#pragma once
#include <string>
#include "Section.h"

class Player;

class Opponent :
    public Section
{
public:
    Opponent(int charnumber);
    void printStats() override;
    void attack(Section* target) override;
    int getVit();
};