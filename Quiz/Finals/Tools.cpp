#include "Tools.h"

Tools::Tools(int power, string name)
{
	mItemPower = power;
	mName = name;
}

string Tools::getName()
{
	return mName;
}

int Tools::getItemPower()
{
	return mItemPower;
}
