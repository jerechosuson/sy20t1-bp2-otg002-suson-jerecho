#include "Section.h"

Section::Section()
{
}

int Section::getHp()
{
	return mHp;
}

int Section::setHp(int damage)
{
	mHp -= damage;
	if (mHp < 0) mHp = 0;
	return mHp;
}

int Section::getAgi()
{
	return mAgi;
}

int Section::getDex()
{
	return mDex;
}

int Section::getPower()
{
	return mPow;
}


string Section::getClass()
{
	return mClass;
}

void Section::attack(Section* Objective)
{
}

void Section::printStats()
{
}
