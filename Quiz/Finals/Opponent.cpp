#include <iostream>
#include "Opponent.h"
#include "Player.h"

using namespace std;

Opponent::Opponent(int classNumber)
	: Section()
{
	if (classNumber == 1) // FOR GOBLIN
	{
		mClass = "Goblin";

		mMaxHp = 70; // goblin is weakling
		mHp = 70;
		mPow = 15;
		mVit = 2;
		mAgi = 3;
		mDex = 2;
	}

	else if (classNumber == 2) // FOR OGRE
	{
		mClass = "Ogre";

		mMaxHp = 100;
		mHp = 100;
		mPow = 15;
		mVit = 3;
		mAgi = 4;
		mDex = 2;
	}

	else if (classNumber == 3) // FOR ORC
	{
		mClass = "Orc";

		mMaxHp = 150;
		mHp = 150;
		mPow = 20;
		mVit = 4;
		mAgi = 5;
		mDex = 3;
	}

	else if (classNumber == 4) // FOR ORC LORD
	{
		mClass = "Orc Lord";

		mMaxHp = 330;
		mHp = 330;
		mPow = 40;
		mVit = 6;
		mAgi = 4;
		mDex = 3;
	}
}

void Opponent::printStats()
{
	cout << "Opponent:" << endl;
	cout << endl;
	cout << "Monster: " << mClass << endl;
	cout << "HP: " << mHp << endl;
	cout << "Power: " << mPow << endl;
	cout << "Vitality: " << mVit << endl;
	cout << "Agility: " << mAgi << endl;
	cout << "Dexterity: " << mDex << endl;
	cout << "--------------------------" << endl;
	cout << endl;
}

void Opponent::attack(Section* target)
{
	Player* player = (Player*)target;

	int hitRate = ((float)mDex / (float)player->getAgi()) * 100;
	int probability = rand() % 100 + 1;
	if (hitRate < 20) hitRate = 20; // hit rate shouldn't be lower than 20%
	else if (hitRate > 80) hitRate = 80; // and must not exceed 80%

	if (probability >= 1 && probability <= hitRate) // successfully hit will have a call attack
	{
		int damage = (mPow - (player->getVitArmor()));
		if (damage <= 0) damage = 1;

		player->setHp(damage);
		cout << mClass << " has attacked " << player->getName() << " for " << damage << "!" << endl;
	}

	else if (probability > hitRate)
	{
		cout << "The " << mClass << " missed!" << endl;
	}

}

int Opponent::getVit()
{
	return mVit;
}
