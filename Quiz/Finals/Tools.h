#pragma once
#include <string>
#include <iostream>

using namespace std;

class Tools
{
public:
	Tools(int power, string name);
	string getName();
	int getItemPower();

private:
	int mItemPower;
	string mName;
};

