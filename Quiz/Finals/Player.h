#pragma once
#include <string>
#include "Section.h"

using namespace std;

class Tools;
class Opponent;

class Player :
    public Section
{
public:
    // constructor & destructor
    Player(string name, int charnumber);
    ~Player();

    // accessors
    string getName();
    int getVitArmor();
    int getLevel();
    int getExp();
    int getX();
    int getY();

    // behaviors
    void attack(Section* objective) override;
    void getExpGold(Opponent* objective);
    void enterShop();
    void changeArmor(string name);
    void changeWeapon(string name);
    void move();
    void rest();
    void printStats() override;
    void enterFight(Opponent* Opponent);

private:
    string mName;
    int mGold;
    int mLevel = 1;
    int mExp;
    int x = 0;
    int y = 0;
    Tools* mWeapon;
    Tools* mArmor;
};


