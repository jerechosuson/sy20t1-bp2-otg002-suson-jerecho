#include "Wizard.h"
#include <iostream>
#include "Magic.h"
#include <string>
#include <time.h>

using namespace std;

int main()
{
    int round = 1;

    // Prompt wizard 1
    Wizard* wizard1 = new Wizard();
    wizard1->mName = "Alpha";
    wizard1->mHp = 250;
    wizard1->mMp = 0;
    wizard1->mMaxDamage = 15;
    wizard1->mMinDamage = 10;
    

    // Prompt wizard 2
    Wizard* wizard2 = new Wizard();
    wizard2->mName = "Bravo";
    wizard2->mHp = 250;
    wizard2->mMp = 0;
    wizard2->mMaxDamage = 15;
    wizard2->mMinDamage = 10;

    // Prompt Alpha's magic
    Magic* magic1 = new Magic();
    magic1->mName = "INRI"; // Alpha's magic
    magic1->mMaxDamage = 60;
    magic1->mMinDamage = 40;
    magic1->mMpCost = 50;
    wizard1->equippedMagic = magic1; // magic equip

    // Prompt Bravo's spell
    Magic* magic2 = new Magic();
    magic2->mName = "IPSOS"; // Bravo's magic
    magic2->mMaxDamage = 60;
    magic2->mMinDamage = 40;
    magic2->mMpCost = 50;
    wizard2->equippedMagic = magic2; // magic equip
    cout << "Alpha and Charlie is duel ready..." << endl;
    system("pause");
    cout << "Charlie: Scared, Echo?" << endl;
    system("pause");
    cout << "Alpha: You wish!" << endl;
    system("pause");
    cout << "Delta: On the count of three, give me your best to disarmed the opponent..." << endl;
    system("pause");
    cout << "Delta: One.. two... three!" << endl << endl;
    system("pause");
    system("cls");

    // Duel continuous/infinite until one character is dead (Hp 0)
    while (wizard1->mHp > 0 && wizard2->mHp > 0)
    {

        //Display round number
        cout << "===============" << endl << "Round: " << round << endl << endl;

        //Display their attacks
        wizard1->attack(wizard2);
        wizard1->generateMp();
        cout << endl;

        wizard2->attack(wizard1);
        wizard2->generateMp();

        //Display the result of the round
        cout << "===============" << endl << "Result" << endl;

        cout << wizard1->mName << ": " << endl;
        cout << "HP: " << wizard1->mHp << endl;
        cout << "MP: " << wizard1->mMp << endl;

        cout << "---------------" << endl;

        cout << wizard2->mName << ": " << endl;
        cout << "HP: " << wizard2->mHp << endl;
        cout << "MP: " << wizard2->mMp << endl;
        system("pause");
        system("cls");

        round++;

    }

    // If a character gets 0 hp 
    if (wizard1->mHp <= 0 || wizard2->mHp <= 0)

    {

        if (wizard1->mHp <= 0)
        {

            cout << wizard2->mName << " disarmed " << wizard1->mName << "!" << endl << wizard2->mName << " Duel Victory!" << endl << endl;

        }

        else if (wizard2->mHp <= 0)
        {

            cout << wizard1->mName << " disarmed " << wizard2->mName << "!" << endl << wizard1->mName << " Duel Victory!!" << endl << endl;

        }
    }


    delete wizard1;
    delete wizard2;
    delete magic1;
    delete magic2;

    system("pause");
    return 0;

}