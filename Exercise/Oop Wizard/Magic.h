#pragma once
#include <string>
#include <iostream>

using namespace std;

class Wizard;

class Magic
{
public:
    string mName;
    int mMaxDamage;
    int mMinDamage;
    int mMpCost;

    void activate(Wizard* target, Wizard* caster);

};

