#pragma once
#include <string>
#include <iostream>

using namespace std;

class Magic;

class Wizard
{
public:
	string mName;
	int mHp;
	int mMp;
	int mMaxDamage;
	int mMinDamage;
	Magic* equippedMagic;

	void attack(Wizard* target);
	void generateMp();

};
