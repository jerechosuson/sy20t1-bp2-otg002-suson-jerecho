#include "IronSkin.h"
#include "Unit.h"
IronSkin::IronSkin()
	:Skill("Iron Skin")
{
}

//Display the effect of the skill
void IronSkin::effect(Unit* player)
{
	cout << player->getName() << " used Iron Skin!" << endl << "Vitality increased 2!" << endl;
	player->setVit(player->getVit() + 2);
}
