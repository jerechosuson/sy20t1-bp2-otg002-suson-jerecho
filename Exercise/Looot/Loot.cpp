#include <iostream>
#include <time.h>
#include <vector> 
#include <string>


using namespace std;

struct Item
{
    string name;
    int goldValue;
}

;
void printStorage(const vector<Item*> inventory)
{
    for (int i = 0; i < inventory.size(); i++)
    {
        Item* currentLoot = inventory[i];
        cout << currentLoot->name << " | " << currentLoot->goldValue << " gold" << endl;
    }
}

Item* generateRandomLoot(int& multiplier, int& totalGold)
{
    int randNum = rand() % 5;
    Item* generateItem = new Item;
    cout << "Acquired items: " << endl;
    if (randNum == 0)

    {
        generateItem->name = "Cursed Stone";generateItem->goldValue = 0;
    }


    if (randNum == 1)

    {
        generateItem->name = "Jellopy";generateItem->goldValue = 5 * multiplier;
        totalGold += generateItem->goldValue;
    }

    if (randNum == 2)

    {
        generateItem->name = "Thick Leader";generateItem->goldValue = 25 * multiplier;
        totalGold += generateItem->goldValue;

    }

    if (randNum == 3)

    {
        generateItem->name = "Sharp Talon";generateItem->goldValue = 50 * multiplier;
        totalGold += generateItem->goldValue;

    }

    if (randNum == 4)

    {
        generateItem->name = "Mithril Ore";generateItem->goldValue = 100 * multiplier;
        totalGold += generateItem->goldValue;
    }

    return generateItem;
}

void enterDungeon(vector<Item*>& inventory, bool& enter, int& multiplier, int& gold)
{
    int totalGold = 0; 
   
    while (enter == 1)
    {

        gold -= 25;
        Item* item = generateRandomLoot(multiplier, totalGold);

        if (gold < 0)
        {
            cout << "------------" << endl << "GAME OVER!" << endl << "You dont meet the requirements!" << endl << "------------" << endl;
            return;
        }

        if (item->name == "Cursed Stone")
        {
            cout << "------------" << endl << "GAME OVER!" << endl << "A CURSED STONE ACQUIRED!" << endl << "------------" << endl;
            for (int i = 1; i < inventory.size(); i++)
            {
                inventory.erase(inventory.begin() + i);
            }
            gold = 0;
            return;
        }

        else

        {
            inventory.push_back(item);
            printStorage(inventory);


            multiplier += 1;
            cout << endl << "Gold :" << gold << endl;
            cout << "Total gold in the inventory: " << totalGold << endl;
            cout << "Do you want some loot?" << endl << "[0] No , [1] Yes" << endl;
            cin >> enter;
            cout << endl;
            if (enter == 0)
            {

                gold += totalGold;
                return;
            }
        }
    }
}

int main()
{
    srand(time(NULL));

    vector<Item*> inventory;
    int gold = 345;
    int multiplier = 1;
    bool enter = 1;

    while (gold > 0 && gold < 500)

    {
        cout << "Gold :" << gold << endl;
        cout << "Multiplier: " << multiplier << endl;
        cout << "Do you wish to loot?" << endl << "[0] No , [1] Yes" << endl;
        cin >> enter;
        cout << endl;
        enterDungeon(inventory, enter, multiplier, gold);
    }

    if (gold >= 500)

    {
        cout << "------------" << endl << "GREETINGS!" << endl << "YOU WIN!" << endl << "------------" << endl;
    }

    system("pause");
    return 0;
}